/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.taobao.dingtalk.service;

import com.taobao.dingtalk.client.DingTalkClientToken;

/***
 *
 * @since:dingtalk-api 1.0
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a> 
 * 2019/11/28 9:36
 */
public abstract class DingTalkAbstractService {
    //消息最大长度,不超过2048字节...
    protected static final Long MESSAGE_MAX_SIZE=2048L;

    protected final DingTalkClientToken dingTalkClientToken;

    protected DingTalkAbstractService(DingTalkClientToken dingTalkClientToken) {
        this.dingTalkClientToken = dingTalkClientToken;
    }
}
